#
#makefile
#
CC = gcc
CFLAGS =
LIBS = -lm

calcul: calculate.o main.o
	gcc calculate.o main.o  -o calcul $(LIBS)

calculate.o: calculate.c calculate.h
	gcc -c calculate.c $(CFLAGS)
	
main.o: main.c calculate.h
	gcc -c calculate.c $(CFLAGS)

clean:
	-rm calcul *.o *~
#end makefile

