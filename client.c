#include "common.h"
#define MESSAGE "Hello Server!!!\n"

int
main()
{
  int writefd;
  int msglen;

  printf("FIFO Client..\n");
  if((writefd = open(FIFO_NAME, O_WRONLY)) < 0)
    {
      fprintf(stderr, "%s: Nemozhno otkrit FIFO (%s)\n",
	      __FILE__, strerror(errno));
      exit(-1);
    }
  msglen = strlen(MESSAGE);
  if(fwrite(writefd, MESSAGE, msglen) != msglen)
    {
      fprintf(stderr, "%s: Oshibka zapisi v FIFO (%s)\n",
	      __FILE__, strerror(errno));
      exit(-2);
    }
  pclose(writefd);
  exit(0);
}
